//DOM это дерево нашего документа, состоит из разных узлов, отображает уровни вложености между элементами

function convertArrayToList(incomingArray, parent) {
    let convertedArray = incomingArray.map(function (element) {
        // let newElement = document.write(`<li>${element}</li>`); - работает, но не по ТЗ(добавляет код
        // прямо в поток, а не в нужный нам parent/node)
        let newElement = document.createElement('li');
        newElement.innerHTML = element;
        // return body.append(newElement) - работает, но не по ТЗ
        return document.getElementById(parent).append(newElement);
    })
    return
}

let testArray = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

convertArrayToList(testArray, 'list');

